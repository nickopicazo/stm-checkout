try{Typekit.load({ async: true });}catch(e){}

(function($) {

  $('.form-checkout').parsley({
    errorClass: 'has-error',
    successClass: 'has-success',
    classHandler: function(ParsleyField) {
        return ParsleyField.$element.parents('.form-group');
    },
    errorsContainer: function(ParsleyField) {
        return ParsleyField.$element.parents('.form-group');
    },
    errorsWrapper: '<span class="help-block">',
    errorTemplate: '<div></div>'
  });

  $.listen('parsley:field:error', function(fieldInstance) {
    // get messages & alert
    var arrErrorMsg = ParsleyUI.getErrorsMessages(fieldInstance);
    var errorMsg = arrErrorMsg.join(';');

    if ( $('.alert ul li').hasClass('field-' + fieldInstance.__id__) ) {

      $('.alert ul li.field-' + fieldInstance.__id__).html(fieldInstance.$element.data('label') + ' : ' + errorMsg);

    } else  {

      $('.alert ul').append('<li class="field-' + fieldInstance.__id__ + '">' + fieldInstance.$element.data('label') + ' : ' + errorMsg + '</li>');

    }

    // console.log(errorMsg);

    // get name of the input with error
    console.log(fieldInstance);
  });

  $.listen('parsley:field:success', function(fieldInstance) {

    $('.alert ul li.field-' + fieldInstance.__id__).remove();

  });

  $.listen('parsley:form:error', function(formInstance) {

    $(formInstance.$element).find('.alert').removeClass('hidden');

    console.log(formInstance);

  });

  $.listen('parsley:form:success', function(formInstance) {

    $(formInstance.$element).find('.alert').addClass('hidden');

    console.log(formInstance);

  });

})(jQuery);
