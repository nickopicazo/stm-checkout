<header>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#"><img src="img/stm-logo-white.png" alt="STM Logo"></a>
      </div>
      <div class="navbar-collapse">
        <p class="navbar-text navbar-right">Questions? +852 8170 3744 &nbsp;&nbsp; <a href="#"><i class="fa fa-envelope fa-lg"></i></a> &nbsp;&nbsp; <a href="#" class="btn btn-sm">Login</a></p>
      </div>
    </div>
  </nav>
</header>
