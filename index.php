<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../favicon.ico">

    <title>Starter Template for Bootstrap</title>
    <link href="css/main.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <?php include( 'inc/header.php' ); ?>
    <div id="main">
      <div class="container">
        <div class="tagline">
          <h1 class="text-center">Join STM - The <span>#1 Voted</span> Affiliate Marketing Forum</h1>
          <p class="lead text-center">If you already have an account on our website, please <a href="#">login</a> to continue</p>
        </div>
        <div class="row">
          <div class="col col-sm-12">
            <form class="form-checkout">
              <div class="page-header">
                <h3><i class="fa fa-lock fa-fw fa-lg"></i> Secure Checkout</h3>
                <div class="logos">
                  <img src="img/norton.png" alt="Norton Security">
                  <img src="img/mcafee.png" alt="McAfee Security">
                  <img src="img/e-trust.png" alt="Trust E">
                  <img src="img/accredited-business.png" alt="Accredited Business">
                </div>
                <p class="encryption">256-Bit Secure Encryption status: &nbsp;&nbsp; <span class="label label-success"><i class="fa fa-lock fa-fw fa-lg"></i> Active</span></p>
                <div class="clearfix"></div>
              </div>
              <div class="row removeGutter">
                <div class="col col-md-8">
                  <div class="primary">
                    <section>
                      <h3>1<span>.</span> Choose a Membership Type</h3>
                      <div class="checkbox">
                        <label>
                          <input type="radio" class="option-input" name="type" checked>
                          <div>
                            <h4>Access to STM Forums, Exclusive Tools, Services, Mastermind Groups and Global Meetups</h4>
                            <span class="price">$99.00 per month</span>
                          </div>
                        </label>
                      </div>
                      <hr style="margin-top:30px;margin-bottom:30px;">
                      <div class="checkbox">
                        <label>
                          <input type="radio" class="option-input" name="type">
                          <div>
                            <h4>Vendor/Company Access to STM Forums</h4>
                            <span class="price">$99.00 per month</span>
                            <span class="desc">If you would like to represent your company on STM, we require you to use this product selection. By joining STM Forum as a vendor you agree to abide by our vendor-specific <a href="#">Terms of Service</a>.</span></div>
                        </label>
                      </div>
                    </section>
                    <section>
                      <h3>2<span>.</span> Fill Your Billing Information</h3>
                      <div class="row fixRow">
                        <div class="col col-sm-6">
                          <div class="form-group">
                            <label for="">First Name<span>*</span></label>
                            <p>Enter your first name here</p>
                            <input type="text" class="form-control input-lg" data-label="First Name" placeholder="First Name" data-parsley-required="true">
                          </div>
                        </div>
                        <div class="col col-sm-6">
                          <div class="form-group">
                            <label for="">Last Name<span>*</span></label>
                            <p>Enter your last name here</p>
                            <input type="text" class="form-control input-lg" data-label="Last Name" placeholder="Last Name" data-parsley-required="true">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="">Email Address<span>*</span></label>
                        <p>A confirmation email will be sent to you at this address</p>
                        <input type="email" class="form-control input-lg" data-label="Email Address" placeholder="Email Address" data-parsley-required="true" data-parsley-type="email">
                      </div>
                      <div class="form-group">
                        <label for="">Username<span>*</span></label>
                        <p>it must be 6 or more characters in length may only contain letters, numbers, and underscores</p>
                        <input type="text" class="form-control input-lg" data-label="Username" placeholder="Username" data-parsley-required="true">
                      </div>
                      <div class="row fixRow">
                        <div class="col col-sm-6">
                          <div class="form-group">
                            <label for="">Password<span>*</span></label>
                            <p>Must be 6 or more characters</p>
                            <input type="password" id="password" class="form-control input-lg" data-label="Password" placeholder="Password" data-parsley-required="true">
                          </div>
                        </div>
                        <div class="col col-sm-6">
                          <div class="form-group">
                            <label for="">Confirm Password<span>*</span></label>
                            <p>Must match</p>
                            <input type="password" class="form-control input-lg" data-label="Confirm Password" placeholder="Confirm Password" data-parsley-required="true" data-parsley-equalto="#password">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="">Coupon Code</label>
                        <p></p>
                        <input type="text" class="form-control input-lg" data-label="Coupon Code" placeholder="Coupon Code">
                      </div>
                    </section>
                  </div>
                </div>
                <div class="col col-md-4">
                  <hr class="noMargin hidden-md hidden-lg">
                  <div class="secondary">
                    <section class="summary">
                      <h3 style="margin-bottom:20px;"><i class="fa fa-shopping-cart fa-fw text-app-primary"></i> Cart Summary</h3>
                      <p><strong>All Items:</strong> $99.00 per month</p>
                      <p class="noMargin text-app-primary"><strong>Total:</strong> $99.00 per month</p>
                    </section>
                    <section>
                      <h3>3<span>.</span> Payment Method</h3>
                      <div class="form-group">
                        <label for="">Name on Card<span>*</span></label>
                        <p>Fill your name as it appears on your card</p>
                        <input type="text" class="form-control input-lg" data-label="Name on Card" placeholder="John Doe" data-parsley-required="true">
                      </div>
                      <div class="form-group">
                        <label for="">Card Number<span>*</span></label>
                        <p>Usually a 16-digit number, no dashes or spaces needed.</p>
                        <input type="text" class="form-control input-lg" data-label="Card Number" placeholder="0000000000000000" data-parsley-required="true" data-parsley-type="digits">
                      </div>
                      <div class="row fixRow-10">
                        <div class="col col-sm-4 col-md-12 col-lg-4">
                          <div class="form-group">
                            <label for="">CVV No.<span>*</span></label>
                            <p>3 Digits</p>
                            <div class="input-group input-group-lg">
                              <input type="text" class="form-control" data-label="CVV Number" placeholder="123" aria-describedby="sizing-addon1" data-parsley-required="true" data-parsley-type="digits">
                              <span class="input-group-addon" id="sizing-addon1"><a href="#"><i class="fa fa-question-circle"></i></a></span>
                            </div>
                          </div>
                        </div>
                        <div class="col col-sm-4 col-md-12 col-lg-4">
                          <div class="form-group">
                            <label for="">Exp. Month<span>*</span></label>
                            <p>Choose a month</p>
                            <select class="form-control input-lg" data-label="Expiration Month" data-parsley-required="true">
                              <option value="1">1</option>
                            </select>
                          </div>
                        </div>
                        <div class="col col-sm-4 col-md-12 col-lg-4">
                          <div class="form-group">
                            <label for="">Exp. Year<span>*</span></label>
                            <p>Choose a year</p>
                            <select class="form-control input-lg" data-label="Expiration Year" data-parsley-required="true">
                              <option value="1">1</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group confirm">
                        <div class="checkbox">
                          <label>
                            <input id="check" type="checkbox" class="option-input" name="type" data-label="Terms & Conditions" data-parsley-required="true" data-parsley-checkmin="1" value="1" data-parsley-trigger="click"> <div><p class="noMargin">I have read and agree to the <a href="#">Terms & Conditions</a></p></div>
                          </label>
                        </div>
                      </div>
                      <div class="text-center" style="margin-bottom:25px;">
                        <img src="img/credit-cards.png" alt="" class="img-responsive">
                      </div>
                      <div class="form-group">
                        <button class="btn btn-primary btn-lg btn-block" id="submit"><i class="fa fa-lock fa-fw"></i> Place Order Now</button>
                      </div>
                      <div class="text-center text-light">
                        <a href="#">Privacy Policy</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="#">Refund Policy</a>
                      </div>
                      <div class="clearfix spacer"></div>
                      <div class="alert alert-danger hidden noMargin" role="alert"><ul></ul></div>
                    </section>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.0/parsley.min.js"></script>
    <script src="https://use.typekit.net/suz3cnf.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>
